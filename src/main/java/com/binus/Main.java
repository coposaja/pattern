package com.binus;

public class Main {
    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                if (x <= y) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        System.out.println();

        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                if (x >= y) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
